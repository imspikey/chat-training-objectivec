//
//  WelcomeViewController.swift
//  chat-training
//
//  Created by evermac on 10/8/19.
//  Copyright © 2019 olla. All rights reserved.
//

import UIKit
import ProgressHUD

class WelcomeViewController: UIViewController {

    @IBOutlet var EmailTextFiled: UITextField!
    
    @IBOutlet var retypePasswordTF: UITextField!
    
    @IBOutlet var passwordTextFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //Mark: IBActions
    
    
    @IBAction func loginButtonPress(_ sender: Any) {
        
        
        
        if passwordTextFiled.text != "" && EmailTextFiled.text != "" {
                login()
        }else{
            ProgressHUD.showError("Email and password are requierd")
            return
        }
        
        cleanTextFileds()
        
    }
    
    @IBAction func registerButtonPress(_ sender: Any) {
        
        if passwordTextFiled.text != "" && EmailTextFiled.text != "" &&
            retypePasswordTF.text != ""
            {
                if passwordTextFiled.text == retypePasswordTF.text
                {
            register()
                }
                else{
                    ProgressHUD.showError("Passwords dos not match")
                }
            }
        else{
            ProgressHUD.showError("All fileds are requierd")
            return
        }
        cleanTextFileds()
    }
    
    func login(){
        ProgressHUD.show("Logging...")
        FUser.loginUserWith(email: EmailTextFiled.text! , password: passwordTextFiled.text! ) { (Error) in
            if Error != nil{
               ProgressHUD.showError(Error!.localizedDescription)
                return
            }
            
            self.goToApp()
        }
    }
    
    func goToApp(){
        ProgressHUD.dismiss()
        cleanTextFileds()
        dismisssKeyboard()
        
        performSegue(withIdentifier: "fromwelcomtofinishregistration", sender: nil);
//        navigationController?.performSegue(withIdentifier: "fromwelcomtofinishregistration", sender: {
//        });
    }
    func register()
    {
        FUser.registerUserWith(email: EmailTextFiled.text!, password: passwordTextFiled.text!, firstName: "", lastName: "") { (Error) in
            if Error != nil{
                ProgressHUD.showError(Error!.localizedDescription)
                return
            }
            
            self.goToApp()
        }
    }
   
    @IBAction func tgs(_ sender: Any) {
        
        dismisssKeyboard()
    }
    func dismisssKeyboard(){
        self.view.endEditing(false)
    }
    
    func cleanTextFileds(){
        EmailTextFiled.text  = ""
        passwordTextFiled.text = ""
        retypePasswordTF.text = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "fromwelcomtofinishregistration"{
            
            let vc = segue.destination as! WlcomVC;
            
            vc.password = EmailTextFiled.text;
            vc.email    = passwordTextFiled.text;
            
        }
        
    }
}
