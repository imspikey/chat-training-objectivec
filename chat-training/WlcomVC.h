//
//  WlcomVC.h
//  chat-training
//
//  Created by evermac on 12/23/19.
//  Copyright © 2019 olla. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WlcomVC : UIViewController
{

    UIImage  *avatarImage;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtSurname;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;

@property (strong, nonatomic) IBOutlet UITextField *txtCity;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;

- (IBAction)btnDoneClick:(id)sender;

- (IBAction)btnCancelClick:(id)sender;

@property (strong, nonatomic) NSString *password;
@property (strong , nonatomic) NSString *email;

@end

